export function findLastCar(car_inventory) {
  //exports to ./test/testFindLastCar.js
  if (!Array.isArray(car_inventory) || car_inventory.length == 0) {
    return `Error:422 Invalid Input`;
  }
  let inventory_length = car_inventory.length;
  return `Last car is ${car_inventory[inventory_length - 1]["car_make"]} ${
    car_inventory[inventory_length - 1]["car_model"]
  }.`;
}

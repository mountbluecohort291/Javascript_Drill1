//exports to ./test/testFindCar.js
export function findCar(inventory, car_id) {
  //exports to ./test/testFindcar.js
  // Check if Number is valid and return car_id,year and name.
  if (isNaN(car_id) || !Array.isArray(inventory) || inventory.length == 0) {
    return "Error:422 Invalid Input";
  }
  let foundCar = inventory.filter((elem) => {
    //find the car with given id with filter method
    return elem.id == car_id;
  })[0];

  if (foundCar) {
    return `The Car with id-${car_id} is a ${foundCar.car_make} ${foundCar.car_model} released in the year ${foundCar.car_year}.`;
  } else {
    return `Sorry! Car Not Found.`;
  }
}

export function olderCars(car_list) {
  //exports to ./test/testOlderCars.js;
  if (!car_list) {
    return `Error:422 Invalid Input!`;
  }
  let oldCars = [];
  for (let car = 0; car < car_list.length; car++) {
    if (car_list[car] < 2000) {
      oldCars.push(car_list[car]);
    }
  }
  console.log(oldCars);
  return `The Number of cars older than year 2000 is ${oldCars.length}`;
}

export function carModelsList(car_data) {
  //exports to test/testCarModels.js
  let modelList = [];
  if (!Array.isArray(car_data) || car_data.length == 0) {
    return `Error:422 Invalid Input!`;
  }
  for (let car of car_data) {
    modelList.push(car["car_model"]);
  }

  //Sorting The ModelList array;
  //  modelList.sort()
  let sorted_model_list = sortModels(modelList);

  //print the sorted models list in a numbered column
  let finalNames = "";
  for (let names = 0; names < sorted_model_list.length; names++) {
    finalNames += `${names + 1}: ${modelList[names]} \n`;
  }
  return `CAR MODELS:\n${finalNames}`;
}
//function to sort the models alphabetically
function sortModels(models) {
  let modelList_length = models.length;
  for (let name = 0; name < modelList_length; name++) {
    for (let next = name + 1; next < modelList_length; next++) {
      if (models[name] > models[next]) {
        let temporary_name = models[name];
        models[name] = models[next];
        models[next] = temporary_name;
      }
    }
  }
  return models;
}

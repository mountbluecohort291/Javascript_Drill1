//Return car years in an array

export function carYears(car_list) {
  //exports to .test/testCarYears.js
  if (!Array.isArray(car_list) || car_list.length == 0) {
    return `Error:422 Invalid Input!`;
  }
  let carYearsList = [];
  for (let carData = 0; carData < car_list.length; carData++) {
    carYearsList.push(car_list[carData]["car_year"]);
  }

  return carYearsList;
}

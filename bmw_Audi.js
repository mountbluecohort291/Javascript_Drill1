export function bmw_Audi(carList) {
  //exports to ./test/testBmwAudi.js
  if (!Array.isArray(carList) || carList.length == 0) {
    return `Error:422 Invalid Input!`;
  }
  let relevant_cars = [];
  for (let car of carList) {
    if (car["car_make"] == "Audi" || car["car_make"] == "BMW") {
      relevant_cars.push(car);
    }
  }
  return relevant_cars;
}
